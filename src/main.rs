use std::fs::OpenOptions;
use std::io::Write;
use std::{cmp::Ordering, fs::File};
use icu_collator::{CollatorOptions, Collator};
use xmltree::{Element, XMLNode};

/*
 <?ProcessingInstructionTarget Content?>
 <![CDATA[ this is cdata ]]>
 <!-- This is a comment -->
*/

fn sort_nodes(nodes: &mut Vec<XMLNode>, collator: &Collator) {
    // sort inside
    for node in nodes.iter_mut() {
        if let Some(element) = node.as_mut_element() {
            element.attributes.sort_by(|key1, value1, key2, value2| {
                let mut order = collator.compare(key1, key2);
                if order == Ordering::Equal {
                    order = collator.compare(value1, value2);
                }
                order
            });

            sort_nodes(&mut element.children, collator);
        }
    }

    // sort outside
    if nodes.len() > 1 {
        nodes.sort_by(|a, b| match (a, b) {
            (XMLNode::Element(a), XMLNode::Element(b)) => {
                let mut a_buf = Vec::<u8>::new();
                a.write(&mut a_buf).ok();
                let mut b_buf = Vec::<u8>::new();
                b.write(&mut b_buf).ok();
                let a_str = std::str::from_utf8(&a_buf).unwrap();
                let b_str = std::str::from_utf8(&b_buf).unwrap();
                let order = collator.compare(a_str, b_str);
                //alphanumeric_sort::compare_str(a_str.to_lowercase(), b_str.to_lowercase());
                //println!("\n{}\n{:?}\n{}\n", a_str, order, b_str);
                order
            }
            (_, _) => Ordering::Equal,
        });
    }
}

fn write_nodes<W: Write>(nodes: &Vec<XMLNode>, mut result: W) {
    for node in nodes {
        match node {
            XMLNode::Element(element) => {
                element.write(&mut result).ok();
            }
            XMLNode::Text(text) => {
                result.write_all(text.as_bytes()).ok();
            }
            XMLNode::CData(cdata) => {
                // <![CDATA[, and the closing tag is ]]>
                result.write_all(b"<![CDATA[").ok();
                result.write_all(cdata.as_bytes()).ok();
                result.write_all(b"]]>").ok();
            }
            XMLNode::Comment(comment) => {
                // <!-- This is a comment -->
                result.write_all(b"<!--").ok();
                result.write_all(comment.as_bytes()).ok();
                result.write_all(b"-->").ok();
            }
            XMLNode::ProcessingInstruction(target, None) => {
                // <?PITarget?>
                result.write_all(b"<!--").ok();
                result.write_all(target.as_bytes()).ok();
                result.write_all(b"-->").ok();
            }
            XMLNode::ProcessingInstruction(target, Some(content)) => {
                // <?PITarget PIContent?> - common example: <?xml version="1.0" encoding="UTF-8" ?>
                result.write_all(b"<!--").ok();
                result.write_all(target.as_bytes()).ok();
                result.write_all(&[b' ']).ok();
                result.write_all(content.as_bytes()).ok();
                result.write_all(b"-->").ok();
            }
        };
    }
    if let Err(error) = result.flush() {
        panic!("couldn't flush output! error: {:?}", error);
    };
}

fn open_for_write(path: &str) -> File {
    match OpenOptions::new().write(true).truncate(true).open(path) {
        Ok(file) => file,
        Err(error) => panic!(
            "couldn't open file for writing from path '{path}': {:?}",
            error
        ),
    }
}

fn main() {
    let path = match std::env::args().nth(1) {
        Some(path) => path,
        None => panic!("please pass the path to the src file as first (and only) parameter"),
    };
    let src_file = match OpenOptions::new().read(true).open(&path) {
        Ok(file) => file,
        Err(error) => panic!("couldn't open file for reading from path '{path}': {:?}", error),
    };
    let mut xml = match Element::parse_all(src_file) {
        Ok(xml) => xml,
        Err(error) => panic!("couldn't parse file '{path}' as xml: {:?}", error),
    };

    let mut collator_options = CollatorOptions::new();
    collator_options.strength = Some(icu_collator::Strength::Secondary);
    let collator: Collator =
        Collator::try_new_unstable(&icu_testdata::unstable(), &Default::default(), collator_options)
            .unwrap();

    sort_nodes(&mut xml, &collator);

    match std::env::args().nth(2) {
        None => write_nodes(&xml, open_for_write(&path)),
        Some(s) => {
            if s == "-" {
                write_nodes(&xml, std::io::stdout())
            } else {
                write_nodes(&xml, open_for_write(&s))
            }
        }
    }
}
